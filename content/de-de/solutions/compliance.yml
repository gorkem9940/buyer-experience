---
  title: Kontinuierliche Software-Konformität mit GitLab
  description: Wie du mit GitLab kompatible Anwendungen mit einer sicheren Softwarebereitstellungskette erstellst.
  template: 'industry'
  components:
    - name: 'solutions-hero'
      data:
        note:
          -  Automatisiere die Konformität und verringere Risiken
        title: Software-Konformität mit GitLab
        subtitle: Erstelle Anwendungen, die den gängigen Vorschriften entsprechen, mit einer sicheren Softwarebereitstellungskette.
        aos_animation: fade-down
        aos_duration: 500
        aos_offset: 200
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Ultimate kostenlos testen
          url: /free-trial/
          data_ga_name: free trial
          data_ga_location: hero
        secondary_btn:
          text: Erfahre mehr über die Preise
          url: /pricing/
          data_ga_name: pricing
          data_ga_location: hero
        image:
          image_url: /nuxt-images/solutions/compliance/compliance-hero.jpeg
          image_url_mobile: /nuxt-images/solutions/compliance/compliance-hero.jpeg
          alt: "Bild: GitLab für den öffentlichen Sektor"
          bordered: true
    - name: 'by-industry-intro'
      data:
        intro_text: Diese Unternehmen vertrauen uns
        logos:
          - name: Duncan Aviation Logo
            image: "/nuxt-images/logos/duncan-aviation-logo.svg"
            url: /customers/duncan-aviation/
            aria_label: Link zur Duncan Aviation Kunden-Fallstudie
          - name: Curve Logo
            image: /nuxt-images/logos/curve-logo.svg
            url: /customers/curve/
            aria_label: Link zur Curve Kunden-Fallstudie
          - name: Hilti Logo
            image: /nuxt-images/logos/hilti_logo.svg
            url: /customers/hilti/
            aria_label: Link zur Hilti Kunden-Fallstudie
          - name: The Zebra Logo
            image: /nuxt-images/logos/zebra.svg
            url: /customers/thezebra/
            aria_label: Link zur The Zebra Kunden-Fallstudie
          - name: New10 Logo
            image: /nuxt-images/logos/new10-logo.svg
            url: /customers/new10/
            aria_label: Link zur Kunden-Fallstudie con Conversica
          - name: Chorus Logo
            image: /nuxt-images/logos/chorus-logo.svg
            url: /customers/chorus/
            aria_label: Link zur Kunden-Fallstudie der Bendigo and Adelaide Bank
    - name: 'side-navigation-variant'
      links:
        - title: Übersicht
          href: '#overview'
        - title: Leistungen
          href: '#capabilities'
        - title: Kundenstimmen
          href: '#customers'
        - title: Preise
          href: '#pricing'
        - title: Ressourcen
          href: '#resources'
      slot_enabled: true
      slot_content:
        - name: 'div'
          id: 'overview'
          slot_enabled: true
          slot_content:
            - name: 'by-solution-benefits'
              data:
                title: Vereinfache und automatisiere die Software-Konformität
                image:
                  image_url: "/nuxt-images/solutions/compliance/compliance-overview.png"
                  alt: Kontinuierliche Software-Konformität mit GitLab
                is_accordion: false
                items:
                  - icon:
                      name: devsecops
                      alt: Symbol DevSecOps
                      variant: marketing
                    header: Verwalte Risiken
                    text: Erreiche mehr als nur die Reduzierung von Sicherheitslücken im Code
                  - icon:
                      name: clipboard-check
                      alt: "Symbol: Klemmbrett mit Häkchen"
                      variant: marketing
                    header: Einfach und reibungslos
                    text: Eine integrierte Erfahrung zur Definition, Durchsetzung von und Berichterstattung zur Konformität
                  - icon:
                      name: release
                      alt: "Symbol: Schutzschild mit Häkchen"
                      variant: marketing
                    header: Implementiere Schutzmaßnahmen
                    text: Kontrolliere den Zugriff und implementiere Richtlinien
        - name: 'div'
          id: 'capabilities'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-solutions-block'
              data:
                markdown: true
                subtitle: Schnell, sicher, konform.
                sub_description: ''
                white_bg: true
                sub_image: /nuxt-images/solutions/compliance/compliance-benefits.png
                solutions:
                  - title: Richtlinienmanagement
                    description: Festlegung von Regeln und Richtlinien zur Einhaltung von Konformitäts-Frameworks und gemeinsamen Kontrollen
                    list:
                      - "**Granulare Benutzerrollen und Berechtigungen:** Definiere Benutzerrollen und Berechtigungsstufen, die für deine Organisation sinnvoll sind"
                      - "**Zugriffskontrolle:** Beschränke den Zugriff mit Zwei-Faktor-Authentifizierung und Ablauf-Token"
                      - "**Konformitätseinstellungen:** Definiere und erzwinge Konformitätsrichtlinien für bestimmte Projekte, Gruppen und Benutzer(innen)"
                      - "**Zugangsdatenbestand:** Behalte den Überblick über alle Zugangsdaten, die für den Zugriff auf eine selbstverwaltete GitLab-Instanz verwendet werden können"
                      - "**Geschützte Branches:** Kontrolliere nicht autorisierte Änderungen an bestimmten Branches– einschließlich Erstellen, Pushen und Löschen eines Branches– ohne ausreichende Berechtigungen oder Genehmigungen"
                    link_text: Mehr erfahren
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html?_gl=1*1r05yn6*_ga*NTg0MjExODQyLjE2MTk1MzkzOTQ.*_ga_ENFH3X7M5Y*MTY2NTY1NDE3OC4xNDguMS4xNjY1NjU1ODM3LjAuMC4w#policy-management
                    data_ga_name: policy mnagement
                    data_ga_location: solutions block
                  - title: Konforme Workflow-Automatisierung
                    description: Setze definierte Regeln, Richtlinien und Aufgabentrennung durch und verringere gleichzeitig das Gesamtgeschäftsrisiko
                    list:
                      - "**Konformitäts-Framework-Projektvorlagen:** Erstelle Projekte, die bestimmten Prüfprotokollen wie HIPAA zugeordnet sind, um einen Prüfpfad aufrechtzuerhalten und Konformitätsprogramme zu verwalten"
                      - "**Konformitäts-Framework-Projektlabels:** Wende allgemeine Konformitätseinstellungen mit einem Label auf einfache Weise auf ein Projekt an"
                      - "**Konformitäts-Framework-Pipelines:** Definiere Konformitäts-Jobs, die in jeder Pipeline ausgeführt werden sollten, um sicherzustellen, dass Sicherheits-Scans durchgeführt, Artefakte erstellt und gespeichert oder andere Schritte ausgeführt werden, die aufgrund deiner organisatorischen Anforderungen erforderlich sind"
                    link_text: Mehr erfahren
                    link_url: https://docs.gitlab.com/ee/administration/compliance.html?_gl=1*nbfxzt*_ga*NTg0MjExODQyLjE2MTk1MzkzOTQ.*_ga_ENFH3X7M5Y*MTY2NTY1NDE3OC4xNDguMS4xNjY1NjU2NDIyLjAuMC4w#compliant-workflow-automation
                    data_ga_name: workflow automation
                    data_ga_location: solutions block
                  - title: Audit-Management
                    description: Bereite dich auf Audits vor und verstehe die Ursachen von Tickets besser durch einfachen Zugriff auf Audit-Daten
                    list:
                      - "**[Audit-Ereignisse:](https://docs.gitlab.com/ee/administration/audit_events.html)** Dokumentiere wichtige Ereignisse wie Änderungen an Benutzerberechtigungsstufen und wer eine(n) neue(n) Benutzer(in) hinzugefügt oder entfernt hat"
                      - "**[Streaming-Audit-Ereignisse:](https://docs.gitlab.com/ee/administration/audit_event_streaming.html)** Konsolidiere deine Audit-Protokolle in einem Tool deiner Wahl"
                      - "**[Audit-Berichte:](https://docs.gitlab.com/ee/administration/audit_reports.html)** Beantworte die Fragen von Prüfer(innen), indem du umfassende Berichte, wie beispielsweise zu Instanz-, Gruppen- und Projektereignissen, Identitätswechseldaten sowie Anmelde- und Benutzerereignissen, generierst"
                      - "**[Konformitätsbericht:](https://docs.gitlab.com/ee/user/compliance/compliance_report/)** Erhalte einen Überblick über Konformitätsverstöße sowie die Gründe und Schwere von Verstößen in Merge Requests"
                  - title: Verwaltung von Sicherheitslücken und Abhängigkeiten
                    description: Anzeige, Einstufung, Trends, Dokumentation und Behebung von Sicherheitslücken sowie Abhängigkeiten in deinen Anwendungen
                    list:
                      - "**[Sicherheits-Dashboards:](https://docs.gitlab.com/ee/user/application_security/security_dashboard/)** Greife auf aktuelle Sicherheitsstatusanwendungen zu und initiiere Behebungsmaßnahmen"
                      - "**[Software-Materialliste:](https://docs.gitlab.com/ee/user/application_security/dependency_list/)** Scanne Anwendungs- und Containerabhängigkeiten auf Sicherheitslücken und erstelle eine Leistungsbeschreibung der Software (SBOM) mit den verwendeten Abhängigkeiten"
        - name: 'div'
          id: 'customers'
          slot_enabled: true
          slot_content:
            - name: 'by-industry-quotes-carousel'
              data:
                align: left
                header: |
                  Mit dem Vertrauen von Unternehmen.
                  <br />
                  Von Entwickler(inne)n geliebt.
                quotes:
                  - title_img:
                      url: /nuxt-images/logos/duncan-aviation-logo.svg
                      alt: Duncan Aviation Logo
                    quote: GitLab hat uns dabei geholfen, manuelle Prozesse mithilfe von Pipelines zu automatisieren. Jetzt stellen wir regelmäßig Code bereit und liefern wesentliche Änderungen und Korrekturen viel schneller an unsere Kunden.
                    author: Ben Ferguson
                    position: Senior Programmer, Duncan Aviation
                    ga_carousel: duncan aviation testimonial
                    url: /customers/duncan-aviation/
                  - title_img:
                      url: /nuxt-images/logos/curve-logo.svg
                      alt: Curve Logo
                    quote: Vor unserem Wechsel zu GitLab standen die operativen Teams unter hohem Druck. Es war schwierig, Entwickler(innen) in die Lage zu versetzen, ihre Arbeit effektiv zu erledigen. Die offensichtliche Wahl war, alles zentral zusammenzuführen und in einer einzigen Übersicht anzuzeigen.
                    author: Ed Shelto
                    position: Site Reliability Engineer, Curve
                    ga_carousel: curve testimonial
                    url: /customers/curve/
                  - title_img:
                      url: /nuxt-images/logos/hilti_logo.svg
                      alt: Hilti Logo
                    quote: GitLab ist wie eine Suite gebündelt und wird mit einem sehr ausgeklügelten Installationsprogramm ausgeliefert. Und dann funktioniert es wie von Zauberhand. Das ist sehr schön, wenn man ein Unternehmen ist, das einfach nur alles zum Laufen bringen möchte.
                    author: Daniel Widerin
                    position: Head of Software Delivery, HILTI
                    ga_carousel: hilti testimonial
                    url: /customers/hilti/
        - name: div
          id: 'pricing'
          slot_enabled: true
          slot_content:
            - name: 'tier-block'
              class: 'slp-mt-96'
              id: 'pricing'
              data:
                header: Welcher Tarif ist der richtige für dich?
                cta:
                  url: /pricing/
                  text: Erfahre mehr über die Preise
                  data_ga_name: pricing
                  data_ga_location: free tier
                  aria_label: pricing
                tiers:
                  - id: free
                    title: Free
                    items:
                      - Statische Anwendungssicherheitstests (SAST) und Geheimniserkennung
                      - Ergebnisse in JSON-Datei
                    link:
                      href: /pricing/
                      text: Mehr erfahren
                      data_ga_name: pricing
                      data_ga_location: free tier
                      aria_label: free tier
                  - id: premium
                    title: Premium
                    items:
                      - Statische Anwendungssicherheitstests (SAST) und Geheimniserkennung
                      - Ergebnisse in JSON-Datei
                      - MR-Genehmigungen und häufigere Kontrollen
                    link:
                      href: /pricing/
                      text: Mehr erfahren
                      data_ga_name: pricing
                      data_ga_location: premium tier
                      aria_label: premium tier
                  - id: ultimate
                    title: Ultimate
                    items:
                      - Alle Funktionen von Premium und zusätzlich
                      - Umfassende Sicherheitsscanner umfassen SAST, DAST, Geheimnisse, Abhängigkeiten, Container, IaC, APIs, Cluster-Images und Fuzz-Tests
                      - Umsetzbare Ergebnisse innerhalb der MR-Pipeline
                      - Konformitäts-Pipelines
                      - Sicherheits- und Konformitäts-Dashboards
                      - Vieles mehr
                    link:
                      href: /pricing/
                      text: Mehr erfahren
                      data_ga_name: pricing
                      data_ga_location: ultimate tier
                      aria_label: ultimate tier
                    cta:
                      href: /free-trial/
                      text: Ultimate kostenlos testen
                      data_ga_name: pricing
                      data_ga_location: ultimate tier
                      aria_label: ultimate tier
        - name: 'div'
          id: 'resources'
          slot_enabled: true
          slot_content:
          - name: solutions-resource-cards
            data:
              title: Zugehörige Ressourcen
              align: left
              column_size: 4
              grouped: true
              cards:
                - icon:
                    name: video
                    variant: marketing
                    alt: "Symbol: Video"
                  event_type: "Video"
                  header: Konforme Pipelines
                  link_text: "Jetzt ansehen"
                  image: "/nuxt-images/solutions/compliance/compliant-pipelines.jpeg"
                  href: https://www.youtube.com/embed/jKA_e_jimoI
                  data_ga_name: Compliant pipelines
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: "Symbol: Video"
                  event_type: "Video"
                  header: Continuous software compliance
                  link_text: "Jetzt ansehen"
                  image: "/nuxt-images/solutions/compliance/continuous-software-compliance.jpeg"
                  href: https://player.vimeo.com/video/694891993?h=7768f52e29
                  data_ga_name: Continuous software compliance
                  data_ga_location: resource cards
                - icon:
                    name: video
                    variant: marketing
                    alt: "Symbol: Video"
                  event_type: "Video"
                  header: SBOM and Attestation
                  link_text: "Jetzt ansehen"
                  image: "/nuxt-images/solutions/compliance/sbom-and-attestation.jpeg"
                  href: https://www.youtube.com/embed/wX6aTZfpJv0
                  data_ga_name: SBOM and Attestation
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: "Symbol: E-Book"
                  event_type: "Book"
                  header: "Leitfaden zur Software Supply-Chain-Security"
                  link_text: "Mehr erfahren"
                  image: "/nuxt-images/blogimages/modernize-cicd.jpg"
                  href: https://cdn.pathfactory.com/assets/10519/contents/360915/35d042c6-3449-4d50-b2e9-b08d9a68f7a1.pdf
                  data_ga_name: "Guide to software supply chain security"
                  data_ga_location: resource cards
                - icon:
                    name: ebook
                    variant: marketing
                    alt: "Symbol: E-Book"
                  event_type: "Book"
                  header: "DevSecOps-Umfrage von GitLab"
                  link_text: "Mehr erfahren"
                  image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
                  href: https://cdn.pathfactory.com/assets/10519/contents/432983/c6140cad-446b-4a6c-96b6-8524fac60f7d.pdf
                  data_ga_name: "GitLab DevSecOps survey"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: "Symbol: Blog"
                  event_type: "Blog"
                  header: "Warum Compliance in DevOps wichtig ist"
                  link_text: "Mehr erfahren"
                  href: https://about.gitlab.com/blog/2022/08/15/the-importance-of-compliance-in-devops/
                  image: /nuxt-images/blogimages/hotjar.jpg
                  data_ga_name: "The importance of compliance in DevOps"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    variant: marketing
                    alt: "Symbol: Blog"
                  event_type: "Blog"
                  header: "Die 5 wichtigsten Konformitätsfunktionen von GitLab"
                  link_text: "Mehr erfahren"
                  href: https://about.gitlab.com/blog/2022/07/13/top-5-compliance-features-to-leverage-in-gitlab/
                  image: /nuxt-images/blogimages/cover_image_regenhu.jpg
                  data_ga_name: "Top 5 compliance features to leverage in GitLab"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    alt: "Symbol: Blog"
                    variant: marketing
                  event_type: "Blog"
                  header: "Wie man die Aufgabentrennung durchsetzt und Konformität erreicht"
                  link_text: "Mehr erfahren"
                  href: "https://about.gitlab.com/blog/2022/04/04/ensuring-compliance/"
                  image: /nuxt-images/blogimages/scm-ci-cr.png
                  data_ga_name: "How to enforce separation of duties and achieve compliance"
                  data_ga_location: resource cards
                - icon:
                    name: blog
                    alt: "Symbol: Blog"
                    variant: marketing
                  event_type: "Blog"
                  header: "Was du über DevOps-Audits wissen musst"
                  link_text: "Mehr erfahren"
                  href: "https://about.gitlab.com/blog/2022/08/31/what-you-need-to-know-about-devops-audits/"
                  image: /nuxt-images/blogimages/hemmersbach_case_study.jpg
                  data_ga_name: "What you need to know about DevOps Audits"
                  data_ga_location: resource cards
                - icon:
                    name: report
                    alt: "Symbol: Bericht"
                    variant: marketing
                  event_type: "Analystenbericht"
                  header: "GitLab ist ein Challenger im Gartner Magic Quadrant 2022"
                  link_text: "Mehr erfahren"
                  href: "https://about.gitlab.com/analysts/gartner-ast22/"
                  image: /nuxt-images/blogimages/anwb_case_study_image_2.jpg
                  data_ga_name: "GitLab a challenger in 2022 Gartner Magic Quadrant"
                  data_ga_location: resource cards
    - name: solutions-cards
      data:
        title: Do more with GitLab
        column_size: 4
        link :
          url: /solutions/
          text: Erkunde weitere Lösungen
          data_ga_name: solutions explore more
          data_ga_location: body
        cards:
          - title: DevSecOps
            description: GitLab versetzt deine Teams in die Lage, Geschwindigkeit und Sicherheit in Einklang zu bringen, indem es die Softwarebereitstellung automatisiert und deine Softwarebereitstellungskette durchgängig schützt.
            cta: Mehr erfahren
            icon:
              name: devsecops
              alt: Devsecops Icon
              variant: marketing
            href: /solutions/security-compliance/
            data_ga_name: devsecpps learn more
            data_ga_location: body
          - title: Sicherheit der Softwarebereitstellungskette
            description: Stelle sicher, dass deine Softwarebereitstellungskette sicher und konform ist.
            cta: Mehr erfahren
            icon:
              name: continuous-delivery
              alt: Kontinuierliche Bereitstellung
              variant: marketing
            href: /solutions/supply-chain/
            data_ga_name: software supply chain security learn more
            data_ga_location: body
          - title: Automatisierte Softwarebereitstellung
            description: Automatisierungsgrundlagen für digitale Innovation, Cloud-Native-Transformationen und Anwendungsmodernisierung
            cta: Mehr erfahren
            icon:
              name: continuous-integration
              alt: "Symbol: kontinuierliche Integration"
              variant: marketing
            href: /solutions/delivery-automation/
            data_ga_name: automated software delivery learn more
            data_ga_location: body

