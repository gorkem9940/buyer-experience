title: GitLab Duo
description: Die KI-Funktionen, die deine Workflows unterstützen
video:
  text: Lernen Sie GitLab Duo kennen
  source: https://player.vimeo.com/video/855805049?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479"
hero: 
  note: GitLab Duo
  description: Die KI-Funktionen, die deine Workflows unterstützen
  image: 
    src: /nuxt-images/solutions/ai/GitLab Logo.png
    alt: KI-Logo
  button:
    text: Teste Ultimate kostenlos
    href: https://gitlab.com/-/trial_registrations/new?glm_source=localhost/solutions/ai/&glm_content=default-saas-trial
    variant: secondary
  banner:
    href: /solutions/code-suggestions/
    text: GitLab Duo Code Suggestions now available
journey:
  features:
    - name: KI-gestützte Workflows
      description: Steigere die Effizienz und verkürze die Zykluszeiten mit Hilfe von KI in jeder Phase des Softwareentwicklungszyklus.
    - name: Datenschutz auf Unternehmensniveau
      description: Wir setzen auf einen datenschutzfreundlichen Ansatz, um Unternehmen und regulierte Organisationen bei der Einführung von KI-gestützten Workflows zu unterstützen.
    - name: Einzelanwendung
      description: Eine einzige Anwendung mit integrierter Sicherheit, um mehr Software schneller bereitzustellen, die Transparenz für die Geschäftsführung über alle Wertschöpfungsketten hinweg zu gewährleisten und Kontextwechsel zu verhindern.
    - name: Befähige jede(n) Benutzer(in) bei jedem Schritt
      description: Von der Planung und Erstellung von Code bis hin zu Tests, Sicherheit und Überwachung unterstützen unsere KI-gestützten Workflows Entwickler(innen)-, Sicherheits- und Betriebsteams.
featureCards:
  title: Funktionen von GitLab Duo
  cards: 
    - name: Code-Vorschläge
      description: Ermöglicht es dir, Code effizienter zu schreiben, indem du während der Eingabe Code-Vorschläge siehst.
      icon: ai-code-suggestions
      href: https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html
    - name: Vorgeschlagene Prüfer(innen)
      description: Findet automatisch die richtigen Prüfer(innen) für einen Merge Request und sorgt so für schnellere und hochwertigere Reviews.
      icon: ai-reviewer-suggestions
      href: https://docs.gitlab.com/ee/user/project/merge_requests/reviews/#suggested-reviewers
    - name: Prognose zur Wertschöpfungskette
      description: Sagt Produktivitätskennzahlen voraus und identifiziert Anomalien im gesamten Lebenszyklus deiner Softwareentwicklung.
      icon: ai-value-stream-forecast
      href: https://docs.gitlab.com/ee/user/analytics/value_streams_dashboard.html
    - name: Zusammenfassung von Ticket-Kommentaren
      description: Bringt alle Beteiligten bei langwierigen Diskussionen schnell auf den neuesten Stand, um sicherzustellen, dass alle im Bilde sind.
      icon: ai-issue
      href: https://docs.gitlab.com/ee/user/ai_features.html#summarize-issue-discussions
    - name: Zusammenfassung vorgeschlagener Änderungen zu Merge Requests
      description: Hilft den Verfassern von Merge Requests dabei, sich abzustimmen und zu handeln, indem sie die Auswirkungen ihrer Änderungen effizient kommunizieren.
      icon: ai-merge-request
      href: https://docs.gitlab.com/ee/user/ai_features.html#summarize-my-merge-request-review
    - name: Erklärung zu dieser Sicherheitslücke
      description: Unterstützt Entwickler(inne)n dabei, Sicherheitslücken effizienter zu beheben und ihre Fähigkeiten zu verbessern, damit sie sichereren Code schreiben können.
      icon: ai-vulnerability-bug
      href: https://docs.gitlab.com/ee/user/ai_features.html#explain-this-vulnerability-in-the-web-ui
    - name: Zusammenfassung von Merge Request-Reviews
      description: Ermöglicht eine bessere Übergabe zwischen Autor(inn)en und Prüfer(inne)n und hilft den Prüfer(inne)n dabei, Vorschläge für Merge Requests effizient zu verstehen.
      icon: ai-summarize-mr-review
      href: https://docs.gitlab.com/ee/user/ai_features.html#summarize-merge-request-changes
    - name: Generierung von Tests in Merge Requests
      description: Automatisiert sich wiederholende Aufgaben und unterstützt dich dabei, Bugs frühzeitig zu erkennen.
      icon: ai-tests-in-mr
      href: https://docs.gitlab.com/ee/user/ai_features.html#generate-suggested-tests-in-merge-requests
    - name: GitLab Chat
      description: Hilft dir dabei, nützliche Informationen in großen Textmengen, z. B. in der Dokumentation, schnell zu erfassen.
      icon: ai-gitlab-chat
      href: https://docs.gitlab.com/ee/user/ai_features.html#gitlab-duo-chat
    - name: Erkläre diesen Code
      description: Ermöglicht es dir, dich schnell einzuarbeiten, indem du eine Erläuterung zum Quellcode erhältst.
      icon: ai-code-explaination
      href: https://docs.gitlab.com/ee/user/ai_features.html#explain-selected-code-in-the-web-ui
principles:
  title: Wir helfen dir dabei, schneller sichere Software zu entwickeln
  slides: 
    - name: Datenschutz durch Design
      description: Effizienz auf Kosten des Datenschutzes, der Sicherheit und der Einhaltung von Vorschriften ist sowohl für dich als auch für uns ein No-Go. Mittels Code-Vorschlägen bleibt dein geschützter Code in der Cloud-Infrastruktur von GitLab sicher und wird nicht als Datensatz in Schulungen verwendet.
      image:
        src: /nuxt-images/solutions/ai/principle-1.png
    - name: Transparenz als Kernstück
      description: Wir geben dir alle wichtigen Informationen über unsere KI-gestützten Funktionen und die Art und Weise, wie wir deine Daten nutzen und schützen.
      image:
        src: /nuxt-images/solutions/ai/principle-2.png
    - name: Jede(r) kann beitragen
      description: Unser offenes Geschäftsmodell ermöglicht es uns, mit unseren Kunden zusammenzuarbeiten, die neue Funktionen zu unserem Produkt beitragen können.
      image:
        src:  /nuxt-images/solutions/ai/principle-3.png
resources:
    data:
      title: Was ist neu in GitLab KI
      column_size: 4
      cards:
        - icon:
            name: blog
            variant: marketing
            alt: blog Icon
          event_type: Blog
          header: AI/ML in DevSecOps Series
          link_text: Mehr lesen
          image: /nuxt-images/blogimages/ai-ml-in-devsecops-blog-series.png
          href: /blog/2023/04/24/ai-ml-in-devsecops-series/
          data_ga_name: AI/ML in DevSecOps Series
          data_ga_location: body
        - icon:
            name: blog
            variant: marketing
            alt: blog Icon
          event_type: Blog
          header: >-
            GitLab details AI-assisted features in the DevSecOps platform
          link_text: Mehr lesen
          href: https://about.gitlab.com/blog/2023/05/03/gitlab-ai-assisted-features/
          image: /nuxt-images/blogimages/ai-fireside-chat.png
          data_ga_name: GitLab details AI-assisted features in the DevSecOps platform
          data_ga_location: body
        - icon:
            name: blog
            variant: marketing
            alt: blog Icon
          event_type: Blog
          header: How AI-assisted code suggestions will advance DevSecOps
          link_text: Mehr lesen
          href: https://about.gitlab.com/blog/2023/03/23/ai-assisted-code-suggestions/
          image: /nuxt-images/blogimages/ai-experiment-stars.png
          data_ga_name: How AI-assisted code suggestions will advance DevSecOps
          data_ga_location: body
report_cta:
  layout: "dark"
  title: Analystenberichte
  reports:
  - description: "GitLab als einziger Marktführer in The Forrester Wave™ anerkannt: Integrierte Software-Entwicklungsplattformen, Q2 2023"
    url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
    link_text: Lesen Sie den Bericht
  - description: "GitLab als Marktführer im Gartner® Magic Quadrant 2023™ für DevOps-Plattformen anerkannt"
    url: /gartner-magic-quadrant/
    link_text: Lesen Sie den Bericht