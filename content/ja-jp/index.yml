---
  title: DevSecOpsプラットフォーム
  description: 計画から本番まで、1つのアプリケーションでチームをまとめます。 より効率的にセキュアなコードを送信して、より速く価値を提供します。
  schema_org: >
    {"@context":"https://schema.org","@type": "Corporation","name":"GitLab","legalName":"GitLab Inc.","tickerSymbol":"GTLB","url":"https://about.gitlab.com","logo":"https://gitlab.com/gitlab-com/gitlab-artwork/raw/master/logo/logo.png", "description" : "GitLabはDevOpsプラットフォームであり、セキュリティとコンプライアンスを強化しながら、ソフトウェアを迅速かつ効率的に提供することにより、ソフトウェア開発の全体的なリターンを最大限に高められます。 GitLabを使用すると、組織内のすべてのチームが協力してソフトウェアを計画、構築、保護、およびデプロイし、完全な透明性、一貫性、およびトレーサビリティでビジネス成果をより速く推進できます。GitLabは、3000万人の推定登録ユーザーと100万人以上のアクティブライセンスユーザーを持つソフトウェア開発ライフサイクル向けのソフトウェアを開発するオープンコアカンパニーであり、2,500人以上のコントリビューターからなるアクティブなコミュニティを持っています。 GitLabはほとんどの企業よりも多くの情報を公開しており、デフォルトでは公開されています。つまり、プロジェクト、戦略、方向性、および指標はオープンに議論されており、ウェブサイト内で見つけられます。 私たちは、コラボレーション、結果、効率性、多様性、インクルージョンとビロンギング、イテレーション、透明性(クレジット)に価値を置いており、これらが私たちのカルチャーを形成しています。","foundingDate":"2011","founders":[{"@type":"Person","name":"Sid Sijbrandij"},{"@type":"Person","name":"Dmitriy Zaporozhets"}],"slogan": "私たちの使命は、すべてのクリエイティブな仕事をただ読むだけの立場から読み書きすることに変換することで、だれもが貢献できるようになります。","address":{"@type":"PostalAddress","streetAddress":"268 Bush Street #350","addressLocality":"San Francisco","addressRegion":"CA","postalCode":"94104","addressCountry":"USA"},"awards": "Comparively's Best Engineering Team 2021、2021 Gartner Magic Quadrant for Application Security Testing - Challenger、DevOps Dozen award 2019 年の最優秀DevOpsソリューション プロバイダー、451 Researchから451 Firestarter Awardを受賞","knowsAbout": [{"@type": "Thing","name": "DevOps"},{"@type": "Thing","name": "CI/CD"},{"@type": "Thing","name": "DevSecOps"},{"@type": "Thing","name": "GitOps"},{"@type": "Thing","name": "DevOps Platform"}],"sameAs":["https://www.facebook.com/gitlab","https://twitter.com/gitlab","https://www.linkedin.com/company/gitlab-com","https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg"]}
  hero_scroll_gallery:
    controls:
      prev: 前のスライド
      next: 次のスライド
    slides:
      - type: block
        title: Software. Faster.
        blurb: GitLabは 最も包括的なAI搭載のDevSecOpsプラットフォームです。
        primaryButton:
          text: 無料トライアルを開始
          link: "https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial"
          data_ga_name: "free trial"
          data_ga_location: "hero"
      - type: card
        image:
          src: '/nuxt-images/home/hero/hero-1.png'
          alt: 'GitLabボードのブランドイメージ'
        title: 業界のトップ企業が 業務や使命の達成に不可欠なソフトウェア開発ツールとして GitLabを選択しています
        button:
          text: GitLabを選ぶ理由
          link: https://about.gitlab.com/why-gitlab/
          data_ga_name: why gitlab
          data_ga_location: hero
      - type: card
        image:
          src: '/nuxt-images/home/hero/hero-2.png'
          alt: 'GitLabボードのブランドイメージ'
        title: AI搭載のワークフローで作業効率を向上させ サイクルタイムを短縮します
        button:
          text: GitLab Duoのご紹介
          link: https://about.gitlab.com/why-gitlab/
          data_ga_name: duo
          data_ga_location: hero
      - type: card
        image:
          src: '/nuxt-images/home/hero/hero-3.png'
          alt: 'GitLabボードのブランドイメージ'
        title: GitLabは DevOpsプラットフォームのマジック・クアドラントで リーダーの1社と評価されました
        button:
          text: レポートを読む
          link: https://about.gitlab.com/gartner-magic-quadrant/
          data_ga_name: Gartner
          data_ga_location: hero
      - type: slide
        image:
          src: "/nuxt-images/home/hero/value-stream-img.svg"
          alt: ""
        title: AI搭載のDevSecOpsプラットフォームを使用して チームの可能性の広がりを体感しませんか？
        blurb: ぜひご利用ください
        primaryButton:
          text: "GitLabへのお問い合わせ"
          link: /sales
          data_ga_name: "sales"
          data_ga_location: "hero"
        secondaryButton:
          text: 無料トライアルを開始
          link: "https://gitlab.com/-/trial_registrations/new?glm_source=about.gitlab.com&glm_content=default-saas-trial"
          data_ga_name: "get started free trial"
          data_ga_location: "hero"
  customer_logos_block:
    text:
      legend: 信頼いただいているお客様：
      url: "/customers/"
      data_ga_name: "trusted by"
      data_ga_location: "home case studies block"
    showcased_enterprises:
      - image_url: "/nuxt-images/home/logo_tmobile_mono.svg"
        link_label: Link to T-Mobile and GitLab webcast landing page"
        alt: "T-Mobileロゴ"
        url: https://learn.gitlab.com/c/learn-how-t-mobile-i?x=04KSqy
        size: sm
      - image_url: "/nuxt-images/home/logo_goldman_sachs_mono.svg"
        link_label: Goldman Sachsお客様事例へのリンク
        alt: "ゴールドマン・サックスロゴ"
        url: "/customers/goldman-sachs/"
        size: sm
      - image_url: "/nuxt-images/software-faster/airbus-logo.png"
        link_label: Airbusお客様事例へのリンク
        alt: "Airbusロゴ"
        url: "/customers/airbus/"
      - image_url: "/nuxt-images/logos/lockheed-martin.png"
        link_label: Link to Lockheed Martin customer case study
        alt: "Lockheed martinロゴ"
        url: /customers/lockheed-martin/
        size: lg
      - image_url: "/nuxt-images/logos/carfax-logo.png"
        link_label: Carfaxの顧客事例へのリンク
        alt: "Carfaxのロゴ"
        url: /customers/carfax/
      - image_url: "/nuxt-images/home/logo_nvidia_mono.svg"
        link_label: Nvidiaお客様事例へのリンク
        alt: "NVIDIAロゴ"
        url: /customers/nvidia/
      - image_url: "/nuxt-images/home/logo_ubs_mono.svg"
        link_label: Link to blogpost How UBS created their own DevSecOps platform using GitLab
        alt: "UBSのロゴ"
        url: https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/
  intro_block:
    title: 単一プラットフォームで スピーディな開発と強靱なセキュリティの双方を実現
    blurb: ソフトウェアデリバリーの自動化や生産性の向上、ソフトウェアサプライチェーン全体のセキュリティ強化が実現可能です。
    primaryButton:
      href: "/platform/"
      text: "プラットフォームについて詳しく読む"
      data_ga_name: "discover the platform"
      data_ga_location: "body"
    secondary_button:
      href: "#"
      text: "GitLabとは？（動画を見る）"
      data_ga_name: "what is gitlab"
      data_ga_location: "body"
      modal:
          video_link: https://player.vimeo.com/video/799236905?h=4eee39a447&badge=0&autopause=0&player_id=0&app_id=58479
          video_title: GitLabとは？（動画を見る）
    image:
      src: "/nuxt-images/home/hero/developer-productivity-img.svg"
      alt: "GitLabボードのブランドイメージ"
  blurb_grid:
    - icon:
        name: devsecops
        size: md
        variant: marketing
      title: ツールチェーンを簡素化
      blurb: <a data-ga-name="platform" data-ga-location="body" href="/platform/">重要なDevSecOpsツール</a>が、すべて1つに集約されています。
    - icon:
        name: agile-large
        size: md
        variant: marketing
      title: ソフトウェアデリバリーを高速化
      blurb: オートメーション、<a data-ga-name="gitlab-duo" data-ga-location="body" href="/gitlab-duo/">AIを活用したワークフロー</a>が利用可能です。
    - icon:
        name: shield-check-large
        size: md
        variant: marketing
      title: セキュリティを統合
      blurb: セキュリティは<a data-ga-name="security-compliance" data-ga-location="body" href="/solutions/security-compliance/">デフォルトでビルトイン。オプションではありません</a>。
    - icon:
        name: gitlab-cloud
        size: md
        variant: marketing
      title: どこにでもデプロイ可能
      blurb: <a data-ga-name='multicloud' data-ga-location='body' href='/topics/multicloud/ '>ベンダーロックイン</a>から解放されましょう。</a>
  feature_showcase:
    - logo:
        src: /nuxt-images/logos/lockheed-martin.png
        alt: ''
      icon:
        name: stopwatch
        size: md
        variant: marketing
      blurb: <a data-ga-name='lockheed-martin' data-ga-location='body' href='/customers/lockheed-martin/'>Lockheed Martin社</a> は GitLabを活用して、時間、コスト、リソースを削減
      button:
        text: 事例を読む
        href: "/customers/lockheed-martin/"
        data_ga_name: read story lockheed martin
        data_ga_location: body
      stats:
        - value: 80倍
          blurb: 速くCIパイプラインをビルド
          icon:
            name: arrow-up
            variant: product
            size: md
        - value: 90%
          blurb: システムメンテナンス時間を短縮
          icon:
            name: arrow-down
            size: md
            variant: product
    - title: ツールチェーンにどれだけコストをかけていますか?
      button:
        text: ROI計算ツールを試す
        href: "/calculator/roi/"
        data_ga_name: roi calculator
        data_ga_location: body
      icon:
        name: time-is-money-alt
        size: md
        variant: marketing
    - title: GitLabを初めて利用される方へ 最初のステップをご案内します
      button:
        text: 導入ステップと効果を見る
        href: "/get-started/"
        data_ga_name: get started
        data_ga_location: body
      icon:
        name: documents
        size: md
        variant: marketing
  tabbed_features:
    header: DevSecOps
    blurb: 計画段階から本番まで すべてのライフサイクルで あらゆるメンバーとのコラボレーションを可能に
    tabs:
      - header: 開発
        header_mobile: Dev
        data_ga_name: development
        data_ga_location: DevSecOps
        blurb: 開発プロセスを自動化することで作業効率を向上させ、セキュリティを損なうことなく時間を有効活用できます。
        features:
          - icon:
              name: continuous-integration
              size: md
              variant: marketing
            text: 継続的インテグレーションとデリバリー
            href: "/features/continuous-integration/"
            data_ga_name: ci
            data_ga_location: development
          - text: GitLab DuoによるAIを活用したワークフロー
            icon:
              name: ai-code-suggestions
              size: md
              variant: marketing
            href: "/solutions/ai/"
            data_ga_name: AI
            data_ga_location: development
          - text: ソースコード管理
            icon:
              name: cog-code
              size: md
              variant: marketing
            href: "/solutions/source-code-management/"
            data_ga_name: source code management
            data_ga_location: development
          - text: 自動化されたソフトウェアデリバリー
            icon:
              name: automated-code
              size: md
              variant: marketing
            href: "/solutions/delivery-automation/"
            data_ga_name: automated software delivery
            data_ga_location: development
      - header: セキュリティ
        header_mobile: Sec
        blurb: セキュリティやコンプライアンス基準に遵守しながらも、業務の効率やスピードを損ないません。
        data_ga_name: security
        data_ga_location: DevSecOps
        features:
          - icon:
              name: shield-check-light
              size: md
              variant: marketing
            text: セキュリティとコンプライアンス
            href: "/solutions/security-compliance/"
            data_ga_name: security and compliance
            data_ga_location: security
          - text: ソフトウェアサプライチェーンの安全性
            icon:
              name: clipboard-check-alt
              size: md
              variant: marketing
            href: "/solutions/supply-chain/"
            data_ga_name: supply chain
            data_ga_location: security
          - text: 統合されたテストと修復
            icon:
              name: monitor-test
              size: md
              variant: marketing
            href: "/solutions/continuous-software-security-assurance/"
            data_ga_name: continuous software security assurance
            data_ga_location: security
          - text: 脆弱性と依存関係の管理
            icon:
              name: monitor-pipeline
              size: md
              variant: marketing
            href: "/solutions/compliance/"
            data_ga_name: compliance
            data_ga_location: セキュリティ
        image:
          src: "/nuxt-images/home/devsecops/security.svg"
          alt: "GitLabボードのブランドイメージ"
      - header: 運用
        header_mobile: Ops
        blurb: "クラウドネイティブ、マルチクラウド、レガシーなどの環境にかかわらず、ソフトウェアのデプロイを自動化し、プロセスを最適化します。"
        data_ga_name: operations
        data_ga_location: devsecops
        image:
          src: "/nuxt-images/home/devsecops/operations.svg"
          alt: "GitLabボードのブランドイメージ"
        features:
          - icon:
              name: digital-transformation
              size: md
              variant: marketing
            text: GitOpsとInfrastructure as Code
            href: "/solutions/gitops/"
            data_ga_name: gitops
            data_ga_location: operations
          - text: 自動化されたソフトウェアデリバリー
            icon:
              name: automated-code
              size: md
              variant: marketing
            href: "/solutions/delivery-automation/"
            data_ga_name: delivery automation
            data_ga_location: operations
          - text: バリューストリーム管理
            icon:
              name: ai-value-stream-forecast
              size: md
              variant: marketing
            href: "/solutions/value-stream-management/"
            data_ga_name: value stream management
            data_ga_location: operations
  recognition_spotlight:
    heading: DevSecOpsプラットフォームの 最先端を切り開くGitLab
    stats:
      - value: 50%以上
        blurb: パフォーマンス向上 (フォーチュン100)
      - value: 3,000万人以上
        blurb: 登録ユーザー数
    cards:
      - logo: /nuxt-images/logos/gartner-logo.svg
        alt: gartnerロゴ
        text: "2023年 Gartner® DevOpsプラットフォームのMagic Quadrant™で GitLabがリーダーの1社に位置付け"
        link:
          text: レポートを読む
          href: /gartner-magic-quadrant/
          data_ga_name: Gartner
          data_ga_location: analyst
      - logo: /nuxt-images/logos/forrester-logo.svg
        alt: forresterロゴ
        text: "Forrester Wave™: 2023年度第二四半期統合ソフトウェア提供プラットフォームで GitLabが唯一のリーダーに認定"
        link:
          text: レポートを読む
          href: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
          data_ga_name: forrester
          data_ga_location: analyst
      - header: "DevSecOpsカテゴリ全体で G2リーダーにランク付けされているGitLab"
        link:
          text: 業界アナリストによるGitLabの評価
          href: /analysts/
          data_ga_name: g2
          data_ga_location: analyst
        badges:
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_Leader_Enterprise_Leader.png
            alt: G2 Enterprise Leader - 2023年夏
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_MomentumLeader_Leader.png
            alt: G2 Momentum Leader - 2023年夏
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_MostImplementable_Total.svg
            alt: G2 Most Implementable - 2023年夏
          - src: /nuxt-images/badges/CloudInfrastructureAutomation_BestResults_Total.png
            alt: G2 Best Results - 2023年夏
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_BestRelationship_Enterprise_Total.png
            alt: G2 Best Relationship Enterprise - 2023年夏
          - src: /nuxt-images/badges/ApplicationReleaseOrchestration_BestRelationship_Mid-Market_Total.svg
            alt: G2 Best Relationship Mid-Market - 2023年夏
          - src: /nuxt-images/badges/CloudInfrastructureAutomation_EasiestToUse_EaseOfUse.png
            alt: G2 Easiest to use - 2023年夏
          - src: /nuxt-images/badges/CloudInfrastructureAutomation_BestUsability_Total.png
            alt: G2 Best Usability - 2023年夏
  quotes_carousel_block:
    controls:
      prev: 前の事例
      next: 次の事例
    quotes:
      - main_img:
          url: /nuxt-images/home/JasonManoharan.png
          alt: Picture of Jason Monoharan
        quote: "「GitLabは戦略的な目標と実際のソフトウェア開発をシームレスに結びつけ、プロジェクトを効果的に進める強力なビジョンを持っています。そのビジョンを実現するために、積極的にプラットフォームの開発と改善に取り組んでいることに感謝します。」"
        author: Jason Monoharan氏
        logo:
          url: /nuxt-images/home/logo_iron_mountain_mono.svg
          alt: ''
        role: テクノロジー部門副社長
        statistic_samples:
          - data:
              highlight: $150,000
              subtitle: コスト削減（年額）
          - data:
              highlight: 20時間
              subtitle: オンボーディング時間を短縮（プロジェクトごと）
        url: customers/iron-mountain/
        header: <a href='customers/iron-mountain/' data-ga-name='iron mountain' data-ga-location='home case studies'>Iron Mountain社</a>は GitLab Ultimateを駆使し DevOpsの進化を牽引
        data_ga_name: iron mountain
        data_ga_location: home case studies
        cta_text: 詳細はこちら
        company: iron mountain
      - main_img:
          url: /nuxt-images/home/HohnAlan.jpg
          alt: Alan Hohnの写真
        quote: "「GitLabに切り替えてデプロイを自動化することで、チームは月に1度、または週に1度だったデリバリーサイクルを、毎日または隔日にまで短縮できました。」"
        author: Alan Hohn氏
        logo:
          url: /nuxt-images/logos/lockheed-martin.png
          alt: ''
        role: ソフトウェア戦略ディレクター
        statistic_samples:
          - data:
              highlight: 80倍
              subtitle: 速くCIパイプラインをビルド
          - data:
              highlight: 90%
              subtitle: システムメンテナンス時間を短縮
        url: /customers/lockheed-martin/
        header: <a href='/customers/lockheed-martin/' data-ga-name='lockheed martin' data-ga-location='home case studies'>Lockheed Martin社</a>は GitLabを使用して、時間、費用、技術者の作業時間を削減
        data_ga_name: lockheed martin case study
        data_ga_location: home case studies
        cta_text: 詳細はこちら
        company: Lockheed martin
      - main_img:
          url: /nuxt-images/home/EvanO_Connor.png
          alt: Picture of Evan O’Connor
        quote: "「GitLabはオープンソースであるため、GitLabエンジニアと直接関わることができ、難しい技術的問題を解決することができました。」"
        author: Evan O’Connor氏
        logo:
          url: /nuxt-images/home/havenTech.png
          alt: ''
        role: プラットフォームエンジニアマネージャー
        statistic_samples:
          - data:
              highlight: 62%
              subtitle: シークレット検出ジョブを実行したユーザー（月ごと）
          - data:
              highlight: 66%
              subtitle: 安全なスキャナジョブを実行したユーザー（月ごと）
        url: /customers/haven-technologies/
        header: <a href='/customers/haven-technologies/' data-ga-name='haven technology' data-ga-location='home case studies'>Haven Technologies社</a>は GitLabを使用して Kubernetesへ移行
        data_ga_name: haven technology
        data_ga_location: home case studies
        cta_text: 詳細はこちら
        company: Haven Technologies
      - main_img:
          url: /nuxt-images/home/RickCarey.png
          alt: Rick Careyの写真
        quote: "「UBSでは、『すべての開発者が同じスピードで待つ』という表現があります。そのため、その待ち時間の短縮は非常に価値のあることです。そして、GitLabによりスムーズで統合された作業環境を体感できます。」"
        author: Rick Carey氏
        logo:
          url: /nuxt-images/home/logo_ubs_mono.svg
          alt: ''
        role: グループチーフテクノロジーオフィサー
        statistic_samples:
          - data:
              highlight: 100万回
              subtitle: 導入後半年間で成功したビルド回数
          - data:
              highlight: 12,000人
              subtitle: GitLabのアクティブユーザー数
        url: https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/
        header: <a href='https://about.gitlab.com/blog/2021/08/04/ubs-gitlab-devops-platform/' data-ga-name='ubs' data-ga-location='home case studies'>UBS社</a>は GitLabを使用して独自のDevOpsプラットフォームを作成
        data_ga_name: ubs
        data_ga_location: home case studies
        cta_text: 詳細はこちら
        company: UBS
      - main_img:
          url: /nuxt-images/home/LakshmiVenkatrama.png
          alt: Lakshmi Venkatramanの写真
        quote: "「GitLabを活用して、チームメンバー間だけでなく、異なるチーム間でも優れたコラボレーションを実現できます。プロジェクトマネージャーとして、プロジェクトやチームメンバーのワークロードを追跡し、プロジェクトの遅延を防げています。いったんプロジェクトが完了すると、パッケージングプロセスを簡単に自動化し、でき上がったものを顧客に送り返せます。そしてGitLabでは、すべてが1つにまとまっているのです。」"
        author: Lakshmi Venkatraman氏
        logo:
          url: /nuxt-images/home/singleron.svg
          alt: ''
        role: プロジェクトマネージャー
        url: https://www.youtube.com/embed/22nmhrlL-FA
        header: Singleron社では GitLabを導入して単一のプラットフォームで連携し合い 患者ケアを改善
        data_ga_name: singleron video
        data_ga_location: home case studies
        cta_text: この事例に関する動画を見る
        modal: true
        comapny: Singleron Biotechnologies
  top-banner:
    text: 5,000人のDevOpsプロフェッショナルからのインサイトを含む、2022年DevSecOpsアンケート結果はこちらです。
    link:
      text: アンケートを見る
      href: '/developer-survey/'
      ga_name: devsecops survey
      icon: gl-arrow-right
