export function softwareFasterDataHelper(pageContent: any[], metadata) {
  return {
    title: metadata[0].fields.ogTitle,
    description: metadata[0].fields.ogDescription,
    title_image: metadata[0].fields.ogImage.fields.image.fields.file.url,
    twitter_image: metadata[0].fields.ogImage.fields.image.fields.file.url,
    hero: {
      title: pageContent[0].fields.title,
      image: {
        url: pageContent[0].fields.backgroundImage.fields.file.url,
      },
      secondary_button: {
        video_url: pageContent[0].fields.video.fields.url,
        text: pageContent[0].fields.primaryCta.fields.text,
        data_ga_name: pageContent[0].fields.primaryCta.fields.dataGaName,
        data_ga_location:
          pageContent[0].fields.primaryCta.fields.dataGaLocation,
      },
    },
    featured_content: {
      col_size: 4,
      header: pageContent[1].fields.header,
      case_studies: pageContent[1].fields.card.map((item) => ({
        header: item.fields.title,
        description: item.fields.description,
        showcase_img: {
          url: item.fields.image.fields.file.url,
          alt: '',
        },
        logo_img: {
          url: item.fields.contentArea[0].fields.image.fields.file.url,
          alt: item.fields.contentArea[0].fields.altText,
        },
        link: {
          video_url: item.fields.contentArea[1]
            ? item.fields.contentArea[1].fields.url
            : false,
          href: item.fields.button.fields.externalUrl
            ? item.fields.button.fields.externalUrl
            : false,
          text: item.fields.button.fields.text,
          data_ga_name: item.fields.button.fields.dataGaName,
          data_ga_location: item.fields.button.fields.dataGaLocation
            ? item.fields.button.fields.dataGaLocation
            : 'body',
        },
      })),
    },
    customer_logos: {
      text: pageContent[2].fields.text,
      showcased_enterprises: pageContent[2].fields.logo.map((item) => ({
        image_url: item.fields.image.fields.file.url,
        alt: item.fields.altText,
        url: item.fields.link,
      })),
    },
    devsecops: {
      header: pageContent[3].fields.title,
      image_url: pageContent[3].fields.backgroundImage.fields.file.url,
      link: {
        text: pageContent[3].fields.primaryCta.fields.text,
        data_ga_name: pageContent[3].fields.primaryCta.fields.dataGaName,
        data_ga_location:
          pageContent[3].fields.primaryCta.fields.dataGaLocation,
      },
      cards: pageContent[4].fields.card.map((item) => ({
        header: item.fields.title,
        description: item.fields.description,
        icon: item.fields.iconName,
      })),
    },
    by_industry_case_studies: {
      title: pageContent[5].fields.header,
      charcoal_bg: true,
      rows: pageContent[5].fields.card.map((item) => ({
        title: item.fields.title,
        subtitle: item.fields.description,
        image: {
          url: item.fields.image.fields.file.url,
          alt: '',
        },
        button: {
          href: item.fields.button.fields.externalUrl,
          text: item.fields.button.fields.text,
          data_ga_name: item.fields.button.fields.dataGaName,
          data_ga_location: item.fields.button.fields.dataGaLocation,
        },
        icon: {
          name: item.fields.iconName,
          alt: '',
        },
      })),
    },
    nextSteps: pageContent[5].fields.variant,
  };
}
