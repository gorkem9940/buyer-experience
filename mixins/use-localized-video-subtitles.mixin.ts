// For Vimeo embed use only
// This adds the 'texttrack' param to the Vimeo URL which will turn
// localized captions on by default IF they exist in Vimeo
import Vue from 'vue';

export const UseLocalizedVideoSubtitles = Vue.extend({
  methods: {
    createVideoSource(video) {
      if (!video || !video?.includes('vimeo.com')) {
        return video;
      }

      const path = this.$route.path.toLowerCase();
      let trackValue;

      if (path.includes('fr-fr')) {
        trackValue = 'fr';
      } else if (path.includes('de-de')) {
        trackValue = 'de';
      } else if (path.includes('ja-jp')) {
        trackValue = 'ja';
      } else trackValue = 'en';

      if (!trackValue) {
        return video;
      }

      const separator = video.includes('?') ? '&' : '?';
      return `${video}${separator}texttrack=${trackValue}`;
    },
  },
});
