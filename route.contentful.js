// File used to generate routes for content files living in Contentful
// This is done to generate those routes during yarn generate
import { createClient } from 'contentful';
import { CONTENT_TYPES } from './common/content-types';

const client = createClient({
  space: process.env.CTF_SPACE_ID,
  accessToken: process.env.CTF_CDA_ACCESS_TOKEN,
  host: 'cdn.contentful.com',
  retryLimit: 1000,
  logHandler: (level, data) => {
    if (data.includes('Rate limit')) {
      return;
    }
  },
});

export async function fetchSupportRoutes() {
  try {
    const response = await client.getEntries({
      content_type: CONTENT_TYPES.PAGE,
      'fields.slug[match]': 'support',
      limit: 100,
    });

    const regex = /^support\/.+/;

    const supportPages = response.items.filter((page) =>
      regex.test(page.fields.slug),
    );

    return supportPages.map((page) => {
      return `/${page.fields.slug}`;
    });
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching Support routes from Contentful:', error);
    throw error;
  }
}

export async function fetchTechnologyPartnerRoutes() {
  try {
    const response = await client.getEntries({
      content_type: CONTENT_TYPES.PAGE,
      'fields.slug[match]': 'partners/technology-partners',
      limit: 100,
    });

    const regex = /^partners\/technology-partners\//;

    const partners = response.items.filter((page) =>
      regex.test(page.fields.slug),
    );

    return partners.map((partner) => {
      return `/${partner.fields.slug}`;
    });
  } catch (error) {
    // eslint-disable-next-line
    console.error(
      'Error fetching Technology Partner routes from Contentful:',
      error,
    );
    throw error;
  }
}

export async function fetchCustomerRoutes() {
  try {
    const caseStudies = await client.getEntries({
      content_type: CONTENT_TYPES.CASE_STUDY,
      select: 'fields.slug',
      limit: 1000,
    });

    return caseStudies.items.map(
      (caseStudy) => `/customers/${caseStudy.fields.slug}`,
    );
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching customer routes from Contentful:', error);
    throw error;
  }
}

export async function fetchTopicsRoutes() {
  try {
    const topics = await client.getEntries({
      content_type: CONTENT_TYPES.TOPICS,
      select: 'fields.slug',
      limit: 1000,
    });

    return topics.items.map((topic) => `${topic.fields.slug}`);
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching topics routes from Contentful:', error);
    throw error;
  }
}

export async function fetchCustomRoutes() {
  try {
    const customPages = await client.getEntries({
      content_type: CONTENT_TYPES.CUSTOM_PAGE,
      select: 'fields.slug',
      limit: 1000,
    });

    return customPages.items.map((page) => `${page.fields.slug}`);
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching custom pages routes from Contentful:', error);
    throw error;
  }
}

export async function fetchStagesDevopsLifecycleRoutes() {
  try {
    const stages = await client.getEntries({
      content_type: 'page',
      'fields.slug[match]': 'stages-devops-lifecycle',
      limit: 100,
    });

    const regex = /^stages-devops-lifecycle\/.+/;

    return stages.items
      .filter((stage) => regex.test(stage.fields.slug))
      .map((stage) => `/${stage.fields.slug}`);
  } catch (error) {
    // eslint-disable-next-line
    console.error(
      'Error fetching stages of the devops lifecycle routes from Contentful:',
      error,
    );
    throw error;
  }
}

export async function fetchSolutionsRoutes() {
  try {
    const solutions = await client.getEntries({
      content_type: 'page',
      'fields.slug[match]': 'solutions',
      limit: 100,
    });

    const regex = /^solutions\/.+/;

    return solutions.items
      .filter((solution) => regex.test(solution.fields.slug))
      .map((solution) => `/${solution.fields.slug}`);
  } catch (error) {
    // eslint-disable-next-line
    console.error('Error fetching solution routes from Contentful:', error);
    throw error;
  }
}

export default [
  '/analysts',
  '/customers/',
  '/company/',
  '/company/all-remote/',
  '/company/contact/',
  '/company/preference-center/',
  '/company/visiting/',
  '/dedicated/',
  '/environmental-social-governance/',
  '/events/summit-las-vegas/',
  '/events/devsecops-world-tour/',
  '/events/devsecops-world-tour/atlanta/',
  '/events/devsecops-world-tour/berlin/',
  '/events/devsecops-world-tour/chicago/',
  '/events/devsecops-world-tour/dallas/',
  '/events/devsecops-world-tour/irvine/',
  '/events/devsecops-world-tour/london/',
  '/events/devsecops-world-tour/melbourne/',
  '/events/devsecops-world-tour/mountain-view/',
  '/events/devsecops-world-tour/new-york/',
  '/events/devsecops-world-tour/paris/',
  '/events/devsecops-world-tour/washington-dc/',
  '/events/devsecops-world-tour/executive/berlin/',
  '/events/devsecops-world-tour/executive/dallas/',
  '/events/devsecops-world-tour/executive/irvine/',
  '/events/devsecops-world-tour/executive/london/',
  '/events/devsecops-world-tour/executive/mountain-view/',
  '/events/devsecops-world-tour/executive/new-york/',
  '/events/devsecops-world-tour/executive/paris/',
  '/events/devsecops-world-tour/executive/washington-dc/',
  '/free-trial/',
  '/free-trial/devsecops/',
  '/get-started/continuous-integration/',
  '/get-started/build-business-case/',
  '/get-started/enterprise/',
  '/get-started/small-business/',
  '/install/ce-or-ee/',
  '/security/',
  '/security/cap/',
  '/solutions/slack/',
  '/solutions/kubernetes/',
  '/solutions/github/',
  '/solutions/jenkins/',
  '/solutions/jira/',
  '/solutions/faster-software-delivery/',
  '/solutions/analytics-and-insights/',
  '/solutions/cloud-native/',
  '/solutions/code-suggestions/',
  '/solutions/startups/',
  '/solutions/startups/join',
  '/solutions/education/',
  '/solutions/education/edu-survey/',
  '/solutions/education/join/',
  '/solutions/gitlab-duo-pro/sales/',
  '/solutions/devops-platform/',
  '/solutions/digital-transformation/',
  '/solutions/nonprofit/',
  '/solutions/nonprofit/join/',
  '/solutions/open-source/',
  '/solutions/open-source/join/',
  '/solutions/open-source/partners/',
  '/topics/',
  '/support',
  '/support/us-government-support',
  '/support/statement-of-support',
  '/support/providing-large-files',
  '/support/general-policies',
  '/support/customer-satisfaction',
  '/support/definitions',
  '/support/scheduling-upgrade-assistance',
  '/support/enhanced-support-offerings',
  '/support/sensitive-information',
  '/support/managing-support-contacts',
  '/support/portal',
  '/support/gitlab-com-policies',
  '/jobs/all-jobs',
];
